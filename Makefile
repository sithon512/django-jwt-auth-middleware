build:
	pipenv run python -m build

clean:
	rm -rfv dist *.egg-info htmlcov

deep-clean: clean
	pipenv --rm

install:
	pipenv install

lock:
	pipenv lock
	pipenv requirements | sed s/^M//g > requirements.txt

test: FORCE
	pipenv run pytest

publish:
	pipenv run python -m twine upload dist/*

FORCE: ;
