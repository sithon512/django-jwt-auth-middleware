from mixer.backend.django import mixer
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from pytest import fixture

from django_jwt_auth_middleware.auth import JWTifier


@fixture
def anon():
    yield AnonymousUser()


@fixture
def user():
    # password is known to test authentication
    user = mixer.blend(get_user_model())
    user.set_password("password")
    user.save()
    yield user
    user.delete()


@fixture
def user_inactive():
    user = mixer.blend(get_user_model(), is_active=False)
    yield user
    user.delete()


@fixture
def jwtifier(user):
    jwtifier = JWTifier(user)
    yield jwtifier
