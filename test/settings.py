from pathlib import Path

USE_TZ = True
SECRET_KEY = "testsecretkey"
BASE_DIR = Path(__file__).resolve().parent.parent

INSTALLED_APPS = [
    # core
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # ours
    "django_jwt_auth_middleware",
]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",  # default
    "django_jwt_auth_middleware.auth.JWTAuthenticationBackend",
]
