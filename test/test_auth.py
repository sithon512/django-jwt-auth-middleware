from pytest import mark, raises

from datetime import timedelta, datetime

from django_jwt_auth_middleware.auth import (
    JWTifier,
    JWTAuthenticationBackend,
    JWTAuthenticationMiddleware,
)
from django_jwt_auth_middleware.settings import DjangoJwtAuthMiddlewareSettings
from .fixtures import anon, user, user_inactive, jwtifier

settings = DjangoJwtAuthMiddlewareSettings()


@mark.django_db
class TestJWTifier:
    def test_create_access(self, jwtifier):
        now = datetime.now()

        # instantiate JWTifier
        access = jwtifier.create_access()
        tok = JWTifier.token_from_str(access)

        # user is the same
        assert jwtifier.user == tok["user"]

        # issuance is close to now: issuance - now < 1 sec
        assert datetime.fromtimestamp(tok["iat"]) - now < timedelta(seconds=1)

        # expiration is correct based on setting: (now + duration) - expiration
        # < 1 sec
        diff = datetime.fromtimestamp(tok["exp"]) - (
            now + timedelta(minutes=settings.JWT_ACCESS_DURATION)
        )
        print("now:", now)
        print("exp:", datetime.fromtimestamp(tok["exp"]))
        assert timedelta(seconds=-1) < diff < timedelta(seconds=1)

        # correct token type was issued
        assert tok["tok"] == "access"

        with raises(TypeError):
            # test correct error thrown when invalid duration type
            jwtifier.create_access(duration=settings.JWT_ACCESS_DURATION)

    def test_create_refresh(self, jwtifier):
        now = datetime.now()

        # instantiate JWTifier
        _, refresh = jwtifier.create_refresh()
        tok = JWTifier.token_from_str(refresh)

        # user is the same
        assert jwtifier.user == tok["user"]

        # issuance is close to now: issuance - now < 1 sec
        assert datetime.fromtimestamp(tok["iat"]) - now < timedelta(seconds=1)

        # expiration is correct based on setting: (now + duration) - expiration
        # < 1 sec
        diff = datetime.fromtimestamp(tok["exp"]) - (
            now + timedelta(days=settings.JWT_REFRESH_DURATION)
        )
        print("now:", now)
        print("exp:", datetime.fromtimestamp(tok["exp"]))
        assert timedelta(seconds=-1) < diff < timedelta(seconds=1)

        # correct token type was issued
        assert tok["tok"] == "refresh"

        with raises(TypeError):
            # test correct error thrown when invalid duration type
            jwtifier.create_refresh(duration=settings.JWT_REFRESH_DURATION)

    def test_validate_token(self, jwtifier):
        # test valid tokens
        access, refresh = jwtifier.create_refresh()
        assert jwtifier.validate_token(access, "access")
        assert jwtifier.validate_token(refresh, "refresh")

        # test invalid labelling
        assert not jwtifier.validate_token(access, "refresh")
        assert not jwtifier.validate_token(refresh, "access")

        # test invalid tokens
        access = jwtifier.create_access(duration=timedelta(minutes=-5))
        assert not jwtifier.validate_token(access, "access")

        _, refresh = jwtifier.create_refresh(duration=timedelta(days=-14))
        assert not jwtifier.validate_token(refresh, "refresh")

    def test_user_from_token(self, jwtifier):
        # test valid token
        token = jwtifier.create_access()
        assert JWTifier.user_from_token(token) == jwtifier.user

        # test invalid token
        assert JWTifier.user_from_token("bad token") is None


@mark.django_db
class TestJWTAuthenticationBackend:
    def test_authenticate(self, jwtifier):
        token = jwtifier.create_access()
        backend = JWTAuthenticationBackend()

        # test successful authentication
        assert (
            backend.authenticate(None, token=f"Bearer {token}") == jwtifier.user
        )

        # test malformed token string
        assert backend.authenticate(None, token=token) is None
        assert backend.authenticate(None, token=f"Basic {token}") is None

        # test invalid token
        token = jwtifier.create_access(duration=timedelta(seconds=-1))
        assert backend.authenticate(None, token=f"Bearer {token}") is None

    def test_get_user(self, user, user_inactive):
        backend = JWTAuthenticationBackend()

        # test successful acquisition
        assert backend.get_user(user.id) == user

        # test unsuccesful acquisition
        assert backend.get_user(user_inactive.id) is None


@mark.django_db
class TestJWTAuthenticationMiddleware:
    def test_process_request(self, user, anon, jwtifier):
        # don't need to fully fake requests
        class Request:
            def __init__(self):
                self.user = anon
                self.META = {}
                self.COOKIES = {}

        # django middleware is instantiated with `get_response` callable
        middleware = JWTAuthenticationMiddleware(lambda x: None)
        token = jwtifier.create_access()

        # test with success via headers
        request = Request()
        request.META = {
            f"HTTP_{settings.JWT_ACCESS_HEADER.upper()}": f"Bearer {token}"
        }
        middleware(request)
        assert request.user == user

        # test with success via cookies
        request = Request()
        request.COOKIES = {settings.JWT_ACCESS_HEADER: f"Bearer {token}"}
        middleware(request)
        assert request.user == user

        # test failure
        request = Request()
        middleware(request)
        assert request.user == anon
