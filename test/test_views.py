from pytest import mark

from datetime import timedelta
from json import dumps as json_dump

from django_jwt_auth_middleware.settings import DjangoJwtAuthMiddlewareSettings
from django_jwt_auth_middleware.views import Login, Logout, Refresh
from .fixtures import anon, user, jwtifier

settings = DjangoJwtAuthMiddlewareSettings()


@mark.django_db
class TestLogin:
    def test_post(self, anon, user):
        class Request:
            def __init__(self):
                self.user = anon
                self.POST = {}
                self.method = "POST"
                self.content_type = "multipart/form-data"

        view = Login.as_view()

        # test successful login
        request = Request()
        request.POST["username"] = user.username
        request.POST["password"] = "password"
        response = view(request)

        assert response.status_code == 200
        assert (
            settings.JWT_ACCESS_HEADER in response.cookies
            and response.cookies.get(settings.JWT_ACCESS_HEADER) != ""
        )
        assert (
            f"Refresh{settings.JWT_ACCESS_HEADER}" in response.cookies
            and response.cookies.get(f"Refresh{settings.JWT_ACCESS_HEADER}")
            != ""
        )

        # test invalid password
        request = Request()
        request.POST["username"] = user.username
        request.POST["password"] = "wrong password"
        response = view(request)

        assert response.status_code == 400
        assert settings.JWT_ACCESS_HEADER not in response.cookies
        assert f"Refresh{settings.JWT_ACCESS_HEADER}" not in response.cookies

        # test login without un/pw, but already authenticated (renew refresh)
        request = Request()
        request.user = user
        response = view(request)

        assert response.status_code == 200
        assert (
            settings.JWT_ACCESS_HEADER in response.cookies
            and response.cookies.get(settings.JWT_ACCESS_HEADER).value != ""
        )
        assert (
            f"Refresh{settings.JWT_ACCESS_HEADER}" in response.cookies
            and response.cookies.get(
                f"Refresh{settings.JWT_ACCESS_HEADER}"
            ).value
            != ""
        )

    def test_post_json(self, anon, user):
        class Request:
            def __init__(self):
                self.user = anon
                self.body = None
                self.POST = {}
                self.method = "POST"
                self.content_type = "application/json"

        view = Login.as_view()

        # test successful login
        request = Request()
        body = {}
        body["username"] = user.username
        body["password"] = "password"
        request.body = json_dump(body).encode()
        response = view(request)

        assert response.status_code == 200
        assert (
            settings.JWT_ACCESS_HEADER in response.cookies
            and response.cookies.get(settings.JWT_ACCESS_HEADER) != ""
        )
        assert (
            f"Refresh{settings.JWT_ACCESS_HEADER}" in response.cookies
            and response.cookies.get(f"Refresh{settings.JWT_ACCESS_HEADER}")
            != ""
        )

        # test invalid password
        request = Request()
        body = {}
        body["username"] = user.username
        body["password"] = "wrong password"
        request.body = json_dump(body).encode()
        response = view(request)

        assert response.status_code == 400
        assert settings.JWT_ACCESS_HEADER not in response.cookies
        assert f"Refresh{settings.JWT_ACCESS_HEADER}" not in response.cookies

        # test login without un/pw, but already authenticated (renew refresh)
        request = Request()
        request.user = user
        response = view(request)

        assert response.status_code == 200
        assert (
            settings.JWT_ACCESS_HEADER in response.cookies
            and response.cookies.get(settings.JWT_ACCESS_HEADER).value != ""
        )
        assert (
            f"Refresh{settings.JWT_ACCESS_HEADER}" in response.cookies
            and response.cookies.get(
                f"Refresh{settings.JWT_ACCESS_HEADER}"
            ).value
            != ""
        )


@mark.django_db
class TestLogout:
    def test_post(self, anon, user):
        class Request:
            def __init__(self):
                self.user = anon
                self.method = "POST"

        view = Logout.as_view()

        # test successful logout
        request = Request()
        request.user = user
        response = view(request)

        assert response.status_code == 200
        assert (
            settings.JWT_ACCESS_HEADER in response.cookies
            and response.cookies.get(settings.JWT_ACCESS_HEADER).value == ""
        )
        assert (
            f"Refresh{settings.JWT_ACCESS_HEADER}" in response.cookies
            and response.cookies.get(
                f"Refresh{settings.JWT_ACCESS_HEADER}"
            ).value
            == ""
        )

        # test failure (not logged in)
        request = Request()
        response = view(request)
        assert response.status_code == 401


@mark.django_db
class TestRefresh:
    def test_post(self, jwtifier):
        class Request:
            def __init__(self):
                self.method = "POST"
                self.META = {}
                self.COOKIES = {}

        view = Refresh.as_view()
        _, refresh = jwtifier.create_refresh()

        # test successful refresh via cookie
        request = Request()
        request.COOKIES[f"Refresh{settings.JWT_ACCESS_HEADER}"] = refresh
        response = view(request)

        assert response.status_code == 200
        assert (
            settings.JWT_ACCESS_HEADER in response.cookies
            and response.cookies.get(settings.JWT_ACCESS_HEADER).value != ""
        )

        # test successful refresh via header
        request = Request()
        request.META[
            f"HTTP_Refresh{settings.JWT_ACCESS_HEADER}".upper()
        ] = refresh
        response = view(request)

        assert response.status_code == 200
        assert (
            settings.JWT_ACCESS_HEADER in response.cookies
            and response.cookies.get(settings.JWT_ACCESS_HEADER).value != ""
        )

        # test unsuccessful refresh
        _, refresh = jwtifier.create_refresh(duration=timedelta(days=-1))
        assert not jwtifier.validate_token(refresh, "refresh")
        request = Request()
        request.COOKIES[f"Refresh{settings.JWT_ACCESS_HEADER}"] = refresh
        response = view(request)

        assert response.status_code == 403
